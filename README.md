# Mars rover
The mars rover was implemented as an HTTP server (ideally running on the rover itself) that receives commands and executes them.

The map is randomly generated every time the server starts. The client doesn't know the map, only it's width and height, and builds his own from the data the rover provides as it moves around.

Tested on Node v10.0.0 and npm v6.1.0

## Disclaimer
I wrote the client (code in `client.js`) just for fun and as quickly as I could. Please don't judge my coding by that :)

## Tests
`npm install` fetches all the dependencies

`npm test` executes the automated tests

## Client
To try out the client:
* run the server with `npm start`
* while the server is up, run the client with `npm run cli` in a different window