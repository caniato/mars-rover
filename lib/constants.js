
const directions = {
  NORTH: 'N',
  SOUTH: 'S',
  WEST:  'W',
  EAST:  'E'
};
Object.freeze(directions);

const terrains = {
  FREE: 'F',
  OBSTACLE: 'X',
  OUTSIDE: 'O'
};
Object.freeze(terrains);

const commands = {
  MOVE_FWD: 'F',
  MOVE_BWD: 'B',
  TURN_CW:  '>',
  TURN_CCW: '<'
};
Object.freeze(commands);

module.exports = {
  directions,
  terrains,
  commands
}
