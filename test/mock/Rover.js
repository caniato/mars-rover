
const dir = require('../../lib/constants').directions;
const Rover = require('../../lib/Rover');

class MockRover extends Rover {
  constructor () {
    super();
    this.pos = { x: 1, y: 0 };
    this.facing = dir.NORTH;
  }

  setPositioning (p) {
    this.pos = p.position;
    this.facing = p.facing;
  }
}

module.exports = MockRover
