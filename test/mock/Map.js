
const _ = require('lodash');
const randInt = require('random-int');

const Map = require('../../lib/Map');

// MockMap representation
//
// 2 F X F F
// 1 F F F F
// 0 X F X F
//   0 1 2 3

class MockMap extends Map {
  constructor () {
    super();
    this._width = 4;
    this._height = 3;
    this._obstacles = [
      { x: 0, y: 0 },
      { x: 2, y: 0 },
      { x: 1, y: 2 }
    ];
  }
}

module.exports = MockMap

