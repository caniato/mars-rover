const cmd = require('./lib/constants').commands;
const dir = require('./lib/constants').directions;

console.log(`
##### MARS ROVER #####
Input commands as a comma separated list.
Avaliable commands: ${Object.values(cmd).join(', ')}
`)

const request = require('superagent');
const repl = require('repl');

repl.start({
  prompt: 'rover> ',
  eval: sendCommands,
  writer: responseBody
});

function sendCommands (commands, context, filename, callback) {
  commands = commands.slice(0, -1).split(',');
  request
    .get('http://localhost:9999/execSeries')
    .send({ commands })
    .set('Authorization', '123')
    .end((err, res) => {
      if (err) return callback(err);
      updateMap(commands, res.body.data);
      callback(null, printMap());
    });
}

function responseBody (map) {
  return map;
}

/////////////////////////////////////
//
let map = {};
let pos = { x: 0, y: 0 };
let facing = dir.NORTH;
let xmax = 10;
let ymax = 6;

function printMap () {
  let print = "";
  for (let y = ymax; y >= 0; y--) {
    for (let x = 0; x <= xmax; x++) {
      if (x === pos.x && y === pos.y) {
        let rov;
        switch (facing) {
          case dir.NORTH:
            rov = '↑'; break;
          case dir.EAST:
            rov = '→'; break;
          case dir.SOUTH:
            rov = '↓'; break;
          case dir.WEST:
            rov = '←';
        }
        print += rov;
      }
      else {
        print += getMapPoint({ x, y }) || '?';
      }
    }
    print += '\n';
  }

  return print;
}

function updateMap (cmds, data) {
  for (let i = 0; i < data.length; i++) {
    execCmd(cmds[i]);

    let sensors = data[i];
    setMapPoint({ x: pos.x, y: pos.y+1 }, sensors[dir.NORTH]);
    setMapPoint({ x: pos.x, y: pos.y-1 }, sensors[dir.SOUTH]);
    setMapPoint({ x: pos.x+1, y: pos.y }, sensors[dir.EAST]);
    setMapPoint({ x: pos.x-1, y: pos.y }, sensors[dir.WEST]);
  }
}

function execCmd (command) {
  switch (command) {
    case cmd.MOVE_FWD:
      move(+1); break;
    case cmd.MOVE_BWD:
      move(-1); break;
    case cmd.TURN_CW:
      turnCW(); break;
    case cmd.TURN_CCW:
      turnCCW();
  }
}

function setMapPoint (point, terrain) {
  normalize(point);
  let key = JSON.stringify(point);
  if (map[key]) return;
  map[key] = terrain;
}

function getMapPoint (point) {
  normalize(point);
  let terrain = map[JSON.stringify(point)];
  if (terrain && terrain.length > 1) terrain.shift();
  return terrain;
}

function turnCW () {
  switch (facing) {
    case dir.NORTH:
      facing = dir.EAST; break;
    case dir.EAST:
      facing = dir.SOUTH; break;
    case dir.SOUTH:
      facing = dir.WEST; break;
    case dir.WEST:
      facing = dir.NORTH;
  }
}

function turnCCW () {
  turnCW(); turnCW(); turnCW();
}

function move(amount) {
  switch (facing) {
    case dir.NORTH:
      pos.y += amount; break;
    case dir.SOUTH:
      pos.y -= amount; break;
    case dir.WEST:
      pos.x -= amount; break;
    case dir.EAST:
      pos.x += amount;
  }

  normalize(pos);
}

function normalize (p) {
    p.x = mod(p.x, xmax+1);
    p.y = mod(p.y, ymax+1);
}

// becaus JS '%' operator doesn't handle negative numbers as desired
function mod(n, m) {
  return ((n % m) + m) % m;
}
