
/**************************************/
/* This server runs on a single rover */
/**************************************/

const PORT = process.env.PORT || 9999;
const app = require('express')();
const async = require('async');
const _ = require('lodash');

const cmd = require('./lib/constants').commands;
const Rover = require('./lib/Rover')
let   rov = new Rover();

// all requests must me authenticated
app.use(require('./lib/auth').checkToken);
// we only receive and send JSON
app.use(require('body-parser').json());

app.get('/sense', (req, res) => {
  rov.getSensorData( (err, sensorData) => {
    if (err) return res.send(err);
    else res.send({ data: sensorData });
  })
});

app.get('/exec', (req, res) => {
  let command = req.body.command;

  // validate request
  if (!(command && isCommand(command)))
    return res.sendStatus(400);

  execute(req.body.command, (err, sensorData) => {
    if (err) return res.send(err);
    else res.send({ data: sensorData });
  })
});

app.get('/execSeries', (req, res) => {
  let commands = req.body.commands;

  // validate request
  if (!(
    commands &&
    Array.isArray(commands) &&
    _.every(commands, isCommand)
  )) return res.sendStatus(400);

  async.mapSeries(req.body.commands, execute, (err, sensorData) => {
    let resBody = {};
    if (err) {
      sensorData.pop();
      resBody.error = err.error;
    }
    resBody.data = sensorData;
    res.send(resBody);
  })
});

app.listen(PORT, () => {
  console.log('Rover up and listening on port ' + PORT)
});

// Execute command a command, results are returned asynchronously
function execute (command, cb) {
  switch (command) {
    case cmd.MOVE_FWD:
      rov.moveForward(cb); break;
    case cmd.MOVE_BWD:
      rov.moveBackwards(cb); break;
    case cmd.TURN_CW:
      rov.turnClockwise(cb); break;
    case cmd.TURN_CCW:
      rov.turnCounterClockwise(cb); break;
    default:
      cb({ error: "Unknown command" });
  }
}

function isCommand (command) {
  return _.includes(cmd, command)
}

function reset () {
  rov = new Rover();
}

module.exports = {
  app,
  rover: rov
} // for testing

